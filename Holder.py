from Crypto.Cipher import AES

globalKey = b"do_what_you_fucking_want_to,_but_don't_change_this_strings"
iv = b"but_why?_are_this_strings_important?_or may be not?_i_don't_know"
globalBlockSize = 32
keySize = 16


def save_to_local(filename, data, key = globalKey[:16]):
    with open(filename, 'wb') as f:
        #print (data)
        # blocks = to_blocks(data)
        # for b in blocks:
        length = 16 - (len(data) % 16)
        data += chr(length) * length
        f.write(encrypt(data, key))

#
# def to_blocks(data, size = globalBlockSize):
#     result = []
#     block = ''
#     s = 0
#     for b in data:
#         block += b
#         s += 1
#         if s >= size:
#             result.append(block)
#             block = ''
#             s = 0
#     if len(block) > 0:
#         for i in range(0, size - len(block)):
#             block += '\x00'
#         result.append(block)
#     return result


def read_from_file(filename, key=globalKey[:16]):
    with open(filename, 'rb') as f:
        result = decrypt(f.read(), key)
        result = result[:-ord(result[-1])]
        return result


def encrypt(data, key=globalKey):
    gamma = AES.new(key[:keySize], AES.MODE_ECB, iv[:16])
    return gamma.encrypt(data)


def decrypt(data, key=globalKey):
    gamma = AES.new(key[:keySize], AES.MODE_ECB, iv[:16])
    return gamma.decrypt(data)


def test():
    str = 'abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789'
    print decrypt(encrypt(str, '0123456789abcdef'), '0123456789abcdef') == str

    source = open("screen.png", 'rb')
    save_to_local("dest", source.read())
    dest = open("destination.png", 'wb')
    dest.write(read_from_file("dest"))
    # how to get the flag
    # c0 = r % 2
    # c1 = g % 2
    # c2 = b % 2
    # a = (c0 + c1) % 2
    # b = (c1 + c2) % 2



if __name__ == '__main__':
    test()
