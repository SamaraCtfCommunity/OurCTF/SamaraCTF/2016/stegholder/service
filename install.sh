#!/bin/sh


sudo apt-get -y install python-dev python-setuptools
sudo apt-get -y install libtiff4-dev libjpeg8-dev zlib1g-dev libfreetype6-dev liblcms1-dev libwebp-dev tcl8.5-dev tk8.5-dev
sudo apt-get install -y libgmp10 libgmp-dev libmpc3 libmpc-dev libmpfr4 libmpfr-dev
