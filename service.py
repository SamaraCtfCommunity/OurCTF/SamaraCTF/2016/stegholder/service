import SocketServer
import cPickle as pickle
import logging
import os
import random
import struct

from Crypto.Cipher import ARC4
from Crypto.Hash import SHA384
from Crypto.PublicKey import RSA

import Holder
import certificate

ADDRESS = '0.0.0.0'
SERVICE_PORT = int(os.getenv('SERVICE_PORT', 7777))
CHECKER_HOST_IP = os.getenv('CHECKER_HOST_IP', '')
PATH_TO_FLAGS = 'flags'
PATH_TO_KEYS = 'keys'
logger = 0

generator = 2
modulus = 0xFFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E088A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D670C354E4ABC9804F1746C08CA237327FFFFFFFFFFFFFFFF


class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass


class ServiceServerHandler(SocketServer.BaseRequestHandler):
    def __init__(self, request, client_address, server):
        SocketServer.BaseRequestHandler.__init__(self, request, client_address, server)

    def handle(self):
        logger.info('Accepted  connection from %s' % self.client_address[0])
        try:
            # authorise checker
            key = read_rsa_key('service.private')
            name = 'service'
            session_key, client_cert = authorise(self.request, certificate.CertificatePrivate(name, key))

            # fourth stage
            recv_message = read_message(self.request)

            if not os.path.exists(PATH_TO_FLAGS):
                os.makedirs(PATH_TO_FLAGS)
            if not os.path.exists(os.path.join(PATH_TO_FLAGS, client_cert.name)):
                os.makedirs(os.path.join(PATH_TO_FLAGS, client_cert.name))

            pos = recv_message.find('||//||')
            if pos == -1:
                raise Exception('Invalid message structure at stage #4')
            message = recv_message[:pos]
            signature = long(recv_message[pos + 6:], 16)

            if not client_cert.verify(message, signature):
                raise CryptoException('Signature verification failed at stage #4')
            pos = recv_message.find(', ')
            if pos == -1:
                raise Exception('ERROR: failed to parse the message')
            user_name = message[:pos]
            encrypted = message[pos + 2:]
            decrypted = decrypt(encrypted, session_key)

            # parsing
            pos = decrypted.split('\\||??//')
            if len(pos) != 2:
                raise Exception('ERROR: failed to parse the decrypted message')
            tup = pos[1].split('\\\\.!.//')
            if len(tup) < 2 or tup[0] == 0 or len(tup[1]) == 0:
                raise Exception('ERROR: failed to parse the message')
            file_name = tup[1]
            cmd = tup[0]
            logger.info('Execute command %s' % cmd)

            if cmd == 'PUT':
                if len(tup) < 3 or len(tup[2]) == 0:
                    raise Exception('ERROR: service don\'t receive file')
                file_str = tup[2]
                Holder.save_to_local(os.path.join(PATH_TO_FLAGS, user_name, file_name), file_str)
                send_message(self.request, 'DONE!')
            if cmd == 'GET':
                file_str = Holder.read_from_file(os.path.join(PATH_TO_FLAGS, user_name, file_name))
                to_send = file_name + ' ' + file_str
                send_message(self.request, encrypt(to_send, session_key))

        except Exception as ex:
            logger.error(str(ex), exc_info=True)
        finally:
            logger.info('Processed connection from %s' % self.client_address[0])
        return


def log_in(socket, private_cert):
    # first stage
    session_key_client, exponent = generate_half_key()
    message = pickle.dumps(private_cert.to_public(), 2) + ',' + hex(session_key_client)
    signature = private_cert.sign(message)
    to_send = message + '||' + hex(signature)
    send_message(socket, to_send)

    # third stage
    recv_message = read_message(socket)
    ind = recv_message.find('||')
    if ind == -1:
        raise CryptoException('Invalid message structure at stage #3')
    message = recv_message[:ind]

    (key_part_str, nonce_str, cert_str) = message.split(', ')
    session_key_serv = long(key_part_str, 16)
    nonce = long(nonce_str, 16)
    server_cert = pickle.loads(cert_str)
    signature = long(recv_message[ind + 2:], 16)

    if not server_cert.verify(message, signature):
        raise CryptoException('Nonce verification failed')

    session_key = pow(session_key_serv, exponent, modulus)

    return session_key, nonce


def authorise(socket, private_cert):
    # second stage
    recv_message = read_message(socket)
    ind = recv_message.find('||')
    if ind == -1:
        raise CryptoException('Invalid message structure at stage #2')

    message = recv_message[:ind]
    signature = long(recv_message[ind + 2:], 16)
    (cert_str, client_part_str) = message.split(', ')
    session_key_client = long(client_part_str, 16)
    client_cert = pickle.loads(cert_str)
    if not client_cert.verify(message, signature):
        raise CryptoException('Signature verification failed at stage #2')

    nonce = generate_nonce()
    session_key_server, exponent = generate_half_key()

    message = hex(session_key_server) + ', ' + hex(nonce) + ', ' + pickle.dumps(private_cert.to_public(), 2)
    to_send = message + '||' + hex(private_cert.sign(message))
    send_message(socket, to_send)

    session_key = pow(session_key_client, exponent, modulus)

    return session_key, client_cert


def generate_nonce():
    return int(random.random() * 0x10000)


def generate_half_key():
    exponent = random.randint(1, modulus - 1)
    return pow(generator, exponent, modulus), exponent


def read_message(s):
    received_buffer = s.recv(4)
    if len(received_buffer) < 4:
        raise Exception('Error while receiving data')
    to_receive = struct.unpack('>I', received_buffer[0:4])[0]
    received_buffer = ''
    while len(received_buffer) < to_receive:
        received_buffer += s.recv(to_receive - len(received_buffer))
    return received_buffer


def send_message(s, message):
    send_buffer = struct.pack('>I', len(message)) + message
    s.sendall(send_buffer)


def encrypt(message, key):
    return ARC4.new(str(key)).encrypt(message)


def decrypt(encrypted, key):
    return ARC4.new(str(key)).decrypt(encrypted)


class CryptoException(Exception):
    pass


def read_rsa_key(key_file):
    try:
        with open(key_file, 'rb') as f:
            return RSA.importKey(f.read())
    except (OSError, IOError) as ex:
        with open(key_file, 'wb') as f:
            key = RSA.generate(2048)
            f.write(key.exportKey())
            return key


def read_key(user_name):
    if os.path.isfile(os.path.join(PATH_TO_KEYS, user_name)):
        with open(user_name, "rb") as f:
            return f.read()[:16]
    else:
        h = SHA384.new(user_name)
        with open(os.path.join(PATH_TO_KEYS, user_name), 'wb') as f:
            f.write(h.digest()[:16])
        return h.digest()[:16]


def get_cert_by_id(id_to_receive):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((CHECKER_HOST_IP, 8888))
    send_message(s, 'GET#' + str(id_to_receive))
    received_str = read_message(s)
    return pickle.loads(received_str)


if __name__ == '__main__':
    logger = logging.getLogger(__name__)
    logging.basicConfig(format='[%(asctime)s] %(levelname)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S',
                        level=logging.INFO)
    address = (ADDRESS, SERVICE_PORT)
    server = ThreadedTCPServer(address, ServiceServerHandler)
    server.serve_forever()
